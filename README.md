# Reflections

Reflections is a stack-based, two-dimensional, esoteric language.
* [Online Interpreter](http://thewastl.gitlab.io/Reflections/reflections.html)
* [Documentation](https://gitlab.com/TheWastl/Reflections/wikis/home)
* [Map of functions](https://thewastl.gitlab.io/Reflections/field.html)
* [Code](https://gitlab.com/TheWastl/Reflections/tree/master#)