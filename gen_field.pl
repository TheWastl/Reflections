#!/usr/bin/perl -w

use List::Util qw(max reduce);

my $i = 0;
my $line;
my $num_args = '';
my @map = ([], [], [], []);
my ($type, $x, $y);

print "const ";

while($line = <STDIN>) {
	if ($line =~
	  /^\s*\[([-+]?\d+)\|([-+]?\d+)\]\s*([A-Za-z0-9_]+)\s*\((\d+)\)\s$/) {
		print 'F_'.uc($3)."=$i,";
		if ($i) { $num_args .= ','; }
		$num_args .= $4;
		$type = 0;
		if ($1 >= 0) {
			$x = $1;
		} else {
			$type++;
			$x = -$1 -1;
		}
		if ($2 >= 0) {
			$y = $2;
		} else {
			$type += 2;
			$y = -$2 -1;
		}
		push @{$map[$type]}, [] while (@{$map[$type]} <= $y);
		push @{$map[$type][$y]}, [ -1, '' ]
		  while (@{$map[$type][$y]} <= $x);
		if ($map[$type][$y][$x][0] != -1) {
			print STDERR "Redeclaration of [$1|$2]\n";
			exit 1;
		}
		$map[$type][$y][$x] = [ $i, $3 ];
		$i++;
	}
}

print "num_args=[$num_args],field=".map_to_js(@map).';';

sub map_to_js {
	my @val = @_;
	if (!@val or ref $val[0] eq 'ARRAY') {
		my $res = '';
		foreach my $tmp (@val) {
			if ($res) { $res .= ','; }
			$res .= map_to_js(@$tmp);
		}
		return "[$res]";
#		return '['.join(',', map { map_to_js(@_) } @val).']';
	} else {
		return $val[0];
	}
}

exit if not @ARGV;

open my $doc, ">", $ARGV[0] or
  die "Cannot open $ARGV[0]";
print $doc '<!doctype html><html><head><title>Reflections Functions</title>'.
	'<link rel="icon" href="reflections.ico">'.
	'<style>.zero{outline:1px solid red}table{text-align:center}</style>'.
	'<link rel="stylesheet" href="reflections.css"></head><body><h2>'.
	'<a href="https://gitlab.com/TheWastl/Reflections#reflections">Reflections</a> Function Map</h2><p><a href='.
'"https://gitlab.com/TheWastl/Reflections/wikis/Coordinate-system#functions">'.
	'More information</a></p><table>';

my $maxx = get_maxx(0, 2);
my $minx = get_maxx(1, 3);

my $maxy = max scalar(@{$map[0]}), scalar(@{$map[1]});
my $miny = max scalar(@{$map[2]}), scalar(@{$map[3]});

print $doc '<col>'x($minx+1)."<col id='zero' class='zero'>".'<col>'x($maxx-1);

print $doc '<tr><th>Functions</th>';
foreach my $val (-$minx..$maxx-1) {
	print $doc "<th>$val</th>";
}
print $doc '</tr>';

foreach my $row ($miny..1) {
	show_row(2, $row-1, -$row);
}

foreach my $row (0..$maxy-1) {
	show_row(0, $row, $row);
}

print $doc '</table></body></html>';
close $doc;

sub get_maxx {
	my @list = ();
	foreach my $tmp (@map[@_]) {
		push @list, @$tmp;
	}
	return 0 if (!@list);
	return scalar @{(reduce { @$a > @$b ? $a : $b } @list)};
}

sub show_row {
	my $t = shift;
	my $row = shift;
	my $n = shift;
	my $zero = shift;
	print $doc '<tr';
	print $doc " class='zero'" if (!$n);
	print $doc "><th>$n</th>";
	my $fill = 0;
	$fill = @{$map[$t+1][$row]} if (@{$map[$t+1]} > $row);
	print $doc '<td></td>' x ($minx - $fill);
	if ($fill) {
		foreach my $f (reverse @{$map[$t+1][$row]}) {
			print $doc "<td>".$f->[1]."</td>";
		}
	}
	if (@{$map[$t]} > $row) {
		foreach my $f (@{$map[$t][$row]}) {
			print $doc "<td>".$f->[1]."</td>";
		}
	}
	print $doc '</tr>';
}
