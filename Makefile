main : index.html field.js field.html

field.html field.js : gen_field.pl field
	perl gen_field.pl field.html < field > field.js

index.html : README.md
	curl -X POST --header 'Content-Type:text/plain' https://api.github.com/markdown/raw -d "`cat README.md`" > index.html

clean :
	rm field.html field.js index.html
